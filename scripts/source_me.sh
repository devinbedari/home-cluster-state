#!/bin/bash

# **README**: Replace these values with your own, then run (from repo root dir):
# $ mkdir -p .env && cp scripts/source_me.sh .env/source_me.sh
# Then reset the secrets to empty, so you don't commit them:
# $ git checkout HEAD -- scripts/source_me.sh
# Source that file every time you need to create/update your secrets:
# $ source .env/source_me.sh

# Tailscale authentication token - long lived, and reuseable.
export TAILSCALE_AUTH_KEY=""

# The base64 encoded ssh private key. Feel free to use:
# CLUSTER_STATE_B64_SSH_KEY=$(cat "path/to/.ssh/private_key" | base64 | tr -d '\n')
export CLUSTER_STATE_B64_SSH_KEY=""

# This is the cloudflare api token to use with external-dns.
# The token needs Zone/Zone:Read Zone/DNS:Edit and access to All Zones
export CLOUDFLARE_API_TOKEN=""
