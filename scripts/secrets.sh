#!/bin/bash

CLOUDFLARE_API_TOKEN=${CLOUDFLARE_API_TOKEN:-"unset"}
CLUSTER_STATE_B64_SSH_KEY=${CLUSTER_STATE_B64_SSH_KEY:-"unset"}
DRY_RUN=${DRY_RUN:-"false"}
DRY_RUN_FLAGS=${DRY_RUN_FLAGS:-""} # Feel free to add --debug if you need it
TAILSCALE_AUTH_KEY=${TAILSCALE_AUTH_KEY:-"unset"}
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

if [ "$DRY_RUN" = "true" ]
then
    DRY_RUN_FLAGS=$DRY_RUN_FLAGS" --dry-run"
fi

helm upgrade ${DRY_RUN_FLAGS} \
    --install argo-cd-repocreds \
    --create-namespace -n argocd \
    --set deployArgo="false" \
    --set ingress=null \
    --set repoCreds.secretName="home-cluster-state-repocreds" \
    --set repoCreds.sourceRepoKey="${CLUSTER_STATE_B64_SSH_KEY}" \
    --set repoCreds.sourceRepo="git@gitlab.com:devinbedari/home-cluster-state.git" \
    "${SCRIPTPATH}/../charts/argo-cd"

helm upgrade ${DRY_RUN_FLAGS} \
    --install tailscale-svc-lb-token \
    --create-namespace -n tailscale-svc-lb \
    --set deployController="false" \
    --set tailscaleAuthKey="${TAILSCALE_AUTH_KEY}" \
    "${SCRIPTPATH}/../charts/tailscale-svc-lb"

helm upgrade ${DRY_RUN_FLAGS} \
    --install externaldns-cloudflare-token \
    --create-namespace -n external-dns \
    --set deployExternalDns="false" \
    --set cloudflareSecret.secretName="cloudflare-secret-token" \
    --set cloudflareSecret.cloudflareApiToken="${CLOUDFLARE_API_TOKEN}" \
    "${SCRIPTPATH}/../charts/external-dns"

helm upgrade ${DRY_RUN_FLAGS} \
    --install certmanager-cloudflare-token \
    --create-namespace -n cert-manager \
    --set cloudflareIssuer=null \
    --set cloudflareToken="${CLOUDFLARE_API_TOKEN}" \
    --set deployCertManager="false" \
    "${SCRIPTPATH}/../charts/cert-manager"
