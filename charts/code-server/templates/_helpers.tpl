{{/* Generate image for code-server */}}
{{- define "codeserver.image" -}}
{{- if .Values.codeserver.deployment.image }}
{{ .Values.codeserver.deployment.image | quote }}
{{- else if eq .Values.codeserver.deployment.os "fedora" }}
fedora:36
{{- else }}
ubuntu:22.04
{{- end }}
{{- end }}

{{/* Define the os type */}}
{{- define "codeserver.os" -}}
{{- if eq .Values.codeserver.deployment.os "fedora" }}
fedora
{{- else }}
ubuntu
{{- end }}
{{- end }}
