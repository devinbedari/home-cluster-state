#!/bin/bash

CODE_SERVER_INSTALL_SCRIPT_URL=${CODE_SERVER_INSTALL_SCRIPT_URL:-"https://code-server.dev/install.sh"}
PACKAGES_TO_INSTALL=${PACKAGES_TO_INSTALL:-"curl"}

# Install tailscale
apt update && apt install ${PACKAGES_TO_INSTALL} -y \
  && curl -fsSL ${CODE_SERVER_INSTALL_SCRIPT_URL} | sh \
  && apt update \
  && apt install tailscale -y \
  && apt clean
