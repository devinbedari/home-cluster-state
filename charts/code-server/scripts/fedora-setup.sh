#!/bin/bash

CODE_SERVER_VERSION=${CODE_SERVER_VERSION:-"4.8.0"}
CODE_SERVER_RELEASE_PATH=${CODE_SERVER_RELEASE_PATH:-"https://github.com/coder/code-server/releases/download"}
PACKAGES_TO_INSTALL=${PACKAGES_TO_INSTALL:-"curl dnf-plugins-core"}

code_server_download_url="${CODE_SERVER_RELEASE_PATH}/v${CODE_SERVER_VERSION}/code-server-${CODE_SERVER_VERSION}-amd64.rpm"
code_server_rpm_filename="code-server-${CODE_SERVER_VERSION}-amd64.rpm"

dnf update -y && dnf install ${PACKAGES_TO_INSTALL} -y \
  && curl -fOL ${code_server_download_url} \
  && rpm -i ${code_server_rpm_filename} \
  && rm -f ${code_server_rpm_filename} \
  && dnf update -y \
  && dnf install tailscale -y \
  && dnf clean all
