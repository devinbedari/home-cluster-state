# Home Cluster-State
The cluster state of my home infra so I can do git-ops instead of backing up `etcd` or having to worry about `docker-desktop` cluster destruction.

## Pre-requisites
You will need:
- [Helm](https://helm.sh/docs/intro/install/).
- A bash shell with basic gnu tools like `tr` and `base64` (git-bash works too for Windows users).

## Initial Deploy
To trigger a deployment to a cluster, run:
```sh
# Load secrets via env vars (Read README comment in scripts/source_me.sh)
source .env/source_me.sh
# Create secrets
bash scripts/secrets.sh
# Deploy argo
helm upgrade --install argocd -n argocd -f charts/argo-cd/values-argocd.yaml --create-namespace charts/argo-cd
# Deploy apps
helm upgrade --install argocd-root-meta -n argocd -f charts/argo-cd/values-def-root.yaml --create-namespace charts/argo-cd
```
